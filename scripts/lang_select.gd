extends Node2D

func _on_ok_pressed():
	get_tree().change_scene('res://scenes/main_menu.tscn')


func _on_EO_pressed():
	singletone.Config.set_value('Lang', 'lang', 'eo')
	singletone.Config.save(singletone.config)


func _on_UK_pressed():
	singletone.Config.set_value('Lang', 'lang', 'uk')
	singletone.Config.save(singletone.config)


func _on_RU_pressed():
	singletone.Config.set_value('Lang', 'lang', 'ru')
	singletone.Config.save(singletone.config)


func _on_EN_pressed():
	singletone.Config.set_value('Lang', 'lang', 'en')
	singletone.Config.save(singletone.config)



func _on_ok1_pressed():
	get_node("langs").show()
