extends Node2D

var node = 'res://scenes/lang_select.tscn'

func _init():
	var lang = singletone.Config.get_value("Lang", "lang")
	if TranslationServer.get_locale() != lang:
		TranslationServer.set_locale(lang)

func _ready():
	var config = ConfigFile.new()
	config.load("user://settings.cfg")
	var highscore = config.get_value("general","highscore",0)
	get_node("high_score").set_text(str(highscore))

func _on_play_pressed():
	if not get_node("AnimationPlayer").is_playing():
		get_node("AnimationPlayer").connect("finished", get_tree(), "change_scene", ["res://scenes/game.tscn"])
		get_node("AnimationPlayer").play("start")

func _on_lang_pressed():
	pass # replace with function body
	get_tree().change_scene(node)
