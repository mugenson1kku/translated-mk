extends Node2D

func _ready():
	if singletone.Config.get_value('Lang', 'lang') == null:
		singletone.is_lang_selected == false
		get_tree().change_scene('res://scenes/lang_select.tscn')
	else:
		singletone.is_lang_selected == true
		get_tree().change_scene('res://scenes/main_menu.tscn')