# Original game
## 1.0.0
First and last release on F-Droid.

# Translated Mighty Knight
## 1.0.0 Alpha 1
* Language selection
* Added next languages: Russian (fully), Esperanto (fully, using on-line translator), Ukraian (partially, using on-line translator)
* Added music
* Added some new texts
* Changed font on Playtime Cyrillic

## 1.0.0 Aplha 2
* Added Spanish language (partially, using on-line translator)
* Fixed error with font in points.tscn
* Ended the Ukraian translation (using on-line translator)
