# Translated Mighty Knight

Mighty Knight translated on Russian, Ukraian, Esperanto and Spanish

## Original game source code avaible here:
https://github.com/alketii/mighty-knight

## Description

Soon..


***

### Version - 1.0.0 Alpha 2

## Visuals


## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

Requirements:
* Godot engine version 2.1.4 or 2.1.7 
* Export templates

## Support


## Original Roadmap
- [ ] Code Cleanup - I designed the game logic while coding
- [x] Audio (P.S. - only in this repository) 
- [x] Sounds (P.S. - only in this repository) 
- [ ] Adding more items
- [ ] Adding more obstacles
- [ ] Android publish

## My Roadmap
- [ ] Add more languages
- [ ] Add more sounds
- [ ] Add more music
- [ ] Rewrite some parts
- [ ] Port it to Desktop

## Contributing


## Authors and acknowledgment
2016-2018 - [alketii](https://github.com/alketii) - Original game
2022 - [MUGEN Son!KKU](https://framagit.org/mugenson1kku) - Russian translation, language selection, something else

## License
This project is licensed under The Expat License.

## Project status
